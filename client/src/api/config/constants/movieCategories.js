export const MovieCategory = Object.freeze({
  POPULAR: "popular",
  NOW_PLAYING: "nowPlaying",
  UPCOMING: "upcoming",
  TOP_RATED: "topRated",
});

export const movieCategoriesRoutingMap = Object.freeze({
  popular: {
    slug: "popular",
    text: "人気の作品",
  },
  nowPlaying: {
    slug: "now-playing",
    text: "上映中",
  },
  upcoming: {
    slug: "upcoming",
    text: "近日公開",
  },
  topRated: {
    slug: "top-rated",
    text: "評価の高い作品",
  },
});
