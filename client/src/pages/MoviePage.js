import React, { useEffect } from "react";
import PropTypes from "prop-types";
import "../styles/MoviePage.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Flag, Grid, Header, Image, Label, List } from "semantic-ui-react";
import CollectionGrid from "../components/CollectionGrid";
import Loader from "../components/Loader";
import NotFound from "../components/NotFound";
import PersonCard from "../components/PersonCard";
import Rating from "../components/Rating";
import {
  createImageSrc,
  findLanguageNameInEnglishFromISO,
} from "../api/config";
import { formatDate } from "../utils/date";
import extractReleaseDatesForRegion from "../utils/extractReleaseDatesForRegion";
import { loadMovieInfo } from "../redux/actions/movieActions";

function MoviePage({ movieId, movie, isFetching, loadMovieInfo }) {
  useEffect(() => {
    loadMovieInfo(
      movieId,
      {
        appendToResponse: ["credits", "release_dates"],
      },
      ["imdb_id"]
    );
  }, [loadMovieInfo, movieId]);

  if (isFetching) {
    return <Loader />;
  }

  if (!movie) {
    return <NotFound />;
  }

  function renderCastItem(person) {
    const { id, name, profile_path: image, character: sub } = person;
    return (
      <PersonCard
        id={id}
        name={name}
        image={image}
        sub={sub}
        as={Link}
        to={`/person/${id}`}
        data-testid="person-card"
      />
    );
  }

  const {
    title,
    overview,
    credits,
    release_date: releaseDate,
    poster_path: imagePath,
    vote_average: voteAverage,
    vote_count: voteCount,
    status,
    original_language: originalLanguage,
    runtime,
    budget,
    revenue,
    genres,
  } = movie;

  const top4Cast = credits ? credits.cast.slice(0, 4) : [];
  const titleDate = releaseDate ? `(${releaseDate.split("-")[0]})` : "";
  const releaseDates = extractReleaseDatesForRegion(movie, "JP");
  const rating = voteCount > 0 ? voteAverage : -1;

  return (
    <div className="MoviePage" data-testid="movie-page">
      <Grid stackable>
        <Grid.Row>
          <Grid.Column width={6}>
            <div className="MoviePage__info__picture-container">
              <Image
                className="MoviePage__info__picture"
                src={createImageSrc({
                  path: imagePath,
                  type: "poster",
                  size: "w500",
                })}
              />
            </div>
          </Grid.Column>
          <Grid.Column width={10}>
            <div className="MoviePage__title">
              <Header size="huge" className="MoviePage__title__name">
                {title}{" "}
                <span className="MoviePage__title__year">{titleDate}</span>
              </Header>
            </div>
            <div className="MoviePage__actions">
              <Rating value={rating} />
            </div>
            <div className="MoviePage__overview">
              <Header size="medium" className="MoviePage__overview__header">
                概要
              </Header>
              <div className="MoviePage__overview__content">
                {overview || "There is not an overview yet."}
              </div>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="MoviePage__cast">
              <CollectionGrid
                title="キャスト"
                collection={top4Cast}
                renderItem={renderCastItem}
                noResultsMessage={"この映画に追加されたキャストはいません。"}
                columns={4}
                loading={false}
                doubling
              />
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <div className="MoviePage__facts">
              <Header size="medium" className="MoviePage__facts_header">
                内容
              </Header>

              <List relaxed="very">
                <List.Item>
                  <List.Header>ステータス</List.Header>
                  {status}
                </List.Item>
                <List.Item>
                  <List.Header>リリース情報</List.Header>
                  {releaseDates.length > 0
                    ? releaseDates.map((date) => (
                        <List.Item key={date}>
                          <Flag name={"JP".toLowerCase()} />
                          {formatDate(date)}
                        </List.Item>
                      ))
                    : "-"}
                </List.Item>
                <List.Item>
                  <List.Header>オリジナル言語</List.Header>
                  {findLanguageNameInEnglishFromISO(originalLanguage)}
                </List.Item>
                <List.Item>
                  <List.Header>上映時間</List.Header>
                  {runtime
                    ? `${Math.floor(runtime / 60)}h ${runtime % 60}m`
                    : "-"}
                </List.Item>
                <List.Item>
                  <List.Header>予算</List.Header>
                  {budget
                    ? `$${budget.toLocaleString("ja-JP", {
                        minimumFractionDigits: 2,
                      })}`
                    : "-"}
                </List.Item>
                <List.Item>
                  <List.Header>収益</List.Header>
                  {revenue
                    ? `$${revenue.toLocaleString("ja-JP", {
                        minimumFractionDigits: 2,
                      })}`
                    : "-"}
                </List.Item>
                <List.Item>
                  <List.Header>ジャンル</List.Header>
                  <Label.Group tag color="blue">
                    {genres && genres.length > 0
                      ? genres // TMDb API returns duplicate genre objects, so remove them
                          .filter(
                            (genre, index, arr) =>
                              arr
                                .map((mapObj) => mapObj.id)
                                .indexOf(genre.id) === index
                          )
                          .map((genre) => (
                            <Label key={genre.id}>{genre.name}</Label>
                          ))
                      : "ジャンルは追加されていません。"}
                  </Label.Group>
                </List.Item>
              </List>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  const movieId = parseInt(ownProps.match.params.id);
  const cachedMovies = state.entities.movies;
  const movie = cachedMovies[movieId] || {};
  const isFetching = typeof movie.imdb_id === "undefined";

  return {
    movieId,
    movie,
    isFetching,
  };
};

MoviePage.propTypes = {
  movieId: PropTypes.number.isRequired,
  movie: PropTypes.object,
  isFetching: PropTypes.bool.isRequired,
  loadMovieInfo: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { loadMovieInfo })(MoviePage);
